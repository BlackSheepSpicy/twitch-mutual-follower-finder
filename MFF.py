#!/usr/bin/env python3
import requests
from sys import argv,stdout,exit
from time import sleep

###REMEMBER TO ADD CLIENT ID###
clientID=""
###############################

if clientID=="":
	print("client-ID is missing, exiting.")
	exit()
#takes a username passed in from execution arguement gets the users info, most notably their User ID as this is the only way to get further info from twitch API
script, userName=argv
url="https://api.twitch.tv/helix/users"
header={'Client-ID': clientID}
getUserInfo=requests.get(url+"?login="+userName, headers=header).json()['data'][0]
userName=getUserInfo['display_name']
userID=getUserInfo['id']

#because twitch only allows 100 follows (this function does both followers and following) to be fetched at a time, we must iterate over each request and consolidate all the names into a list
def GetAll(follow):
	#branch to find those who the target is following
	if follow == "followers":
		param="?to_id="
		query='from_name'
	#branch to find who is following the target
	elif follow == "following":
		param="?from_id="
		query='to_name'
	#its short for pagination, i had to google what that meant tbh
	pag=''
	entryList=[]
	while True:
		r=requests.get(url+'/follows'+param+userID+"&after="+pag,headers=header)
		if r.status_code == 200:
			r=r.json()
			if 'cursor' in r['pagination']:
				pag=r['pagination']['cursor']
				for entry in range(len(r['data'])):
					entryList.append(r['data'][entry][query])
					if len(entryList)!= r['total']:			
						stdout.write(f"{len(entryList)}/{r['total']} {follow} fetched\r")
					else:
						stdout.write(f"{len(entryList)}/{r['total']} {follow} fetched COMPLETE\n")
			else:
				return entryList
		else: 
			sleep(1)	
#invokes GetAll to create list of people who follow the target and people they follow, then compares them and puts mutual followers into a list
followers=GetAll("followers")
following=GetAll("following")
print("Generating List...")
mutual=[]

for i in range(len(followers)):
	for j in range(len(following)):
		if followers[i] == following[j]:
			mutual.append(following[j])
print(f"\nMUTUAL FOLLOW TOTAL: {len(mutual)}")
for m in mutual:
	print("https://twitch.tv/"+m)
		
		
